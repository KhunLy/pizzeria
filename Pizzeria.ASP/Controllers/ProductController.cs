﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Filters;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        [Role("Customer", "Admin")]
        public ActionResult Index()
        {
            IEnumerable<Product> list
                = ServicesLocator.GetService<ProductRepository>()
                .Get();
            IEnumerable<ProductModel> model 
                = list.Select(x => x.MapTo<ProductModel>()); 
            return View(model);
        }

        [HttpGet]
        [Role("Admin")]
        public ActionResult Add()
        {
            return View(new ProductModel());
        }

        [HttpPost]
        [Role("Admin")]
        public ActionResult Add(ProductModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.File != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    model.File.SaveAs(
                        HostingEnvironment
                        .MapPath("~/Content/Images/Products/") + fileName + model.File.FileName);
                    model.Image = "/Content/Images/Products/" + fileName + model.File.FileName;
                }
                ProductRepository repo
                    = ServicesLocator.GetService<ProductRepository>();

                Product p = model.MapTo<Product>();

                repo.Insert(p);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            // Récuperer le connection à la table produit
            ProductRepository repo 
                = ServicesLocator.GetService<ProductRepository>();
            // Récuperer le produit dont l'id est id
            Product p = repo.GetById(id);
            
            if(p == null)
            {
                return new HttpNotFoundResult();
            }
            // Fournir le produit mappé à la vue
            return View(p.MapTo<ProductModel>());
        }

        [HttpPost]
        public ActionResult Edit(ProductModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.File != null)
                {
                    if(model.Image != null)
                    {
                        // retirer l'ancienne image
                        System.IO.File.Delete(HostingEnvironment.MapPath(model.Image));
                    }
                    string fileName = Guid.NewGuid().ToString();
                    model.File.SaveAs(
                        HostingEnvironment
                        .MapPath("~/Content/Images/Products/") + fileName + model.File.FileName);
                    model.Image = "/Content/Images/Products/" + fileName + model.File.FileName;
                }
                ProductRepository repo
                    = ServicesLocator.GetService<ProductRepository>();

                Product p = model.MapTo<Product>();
                repo.Update(p);
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}
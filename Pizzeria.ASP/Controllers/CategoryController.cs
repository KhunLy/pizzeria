﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            // Recuperer les categories dans la db
            //"Server=DESKTOP-HKVTM9G\SQLSERVER;Integrated Security=SSPI",
            CategoryRepository repo 
                = ServicesLocator.GetService<CategoryRepository>();
            IEnumerable<Category> categories = repo.Get();

            // Mapping
            IEnumerable<CategoryModel> model 
                = categories.Select(x => x.MapTo<CategoryModel>());
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(CategoryModel model)
        {
            if(ModelState.IsValid)
            {
                // Récuperer le repo
                CategoryRepository repo 
                    = ServicesLocator.GetService<CategoryRepository>();
                // enregistrer les données en db
                Category c = model.MapTo<Category>();
                repo.Insert(c);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CategoryRepository repo
                    = ServicesLocator.GetService<CategoryRepository>();
            repo.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
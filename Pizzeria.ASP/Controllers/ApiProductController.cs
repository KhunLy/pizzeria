﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pizzeria.ASP.Controllers
{
    public class ApiProductController : ApiController
    {
        [HttpGet]
        public IEnumerable<ProductModel> Get()
        {
            ProductRepository repo 
                = ServicesLocator.GetService<ProductRepository>();
            IEnumerable<ProductModel> model =
                repo.Get().Select(x => x.MapTo<ProductModel>());
            return model;
        }
    }
}

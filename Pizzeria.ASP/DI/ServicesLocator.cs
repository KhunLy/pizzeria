﻿using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using System.Configuration;
using Pizzeria.ASP.Services;
using System.Security.Cryptography;

namespace Pizzeria.ASP.DI
{
    public static class ServicesLocator
    {
        private static ServiceCollection Services = new ServiceCollection();
        private static ServiceProvider Provider;

        static ServicesLocator()
        {
            string connectionString
                = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider
                = ConfigurationManager.ConnectionStrings["default"].ProviderName;
            Services.AddTransient(
                (x) => new CategoryRepository(connectionString, provider)
            );
            Services.AddTransient(
                (x) => new ProductRepository(connectionString, provider)
            );
            Services.AddTransient(
                (x) => new UserRepository(connectionString, provider)
            );
            Services.AddTransient(
                (x) => new OrderLineRepository(connectionString, provider)
            );
            Services.AddTransient(
                (x) => new OrderRepository(connectionString, provider)
            );
            //Services.AddSingleton(
            //    (x) => new HashService(new SHA512CryptoServiceProvider())
            //);
            Services.AddSingleton<HashService>();
            Services.AddSingleton<HashAlgorithm, SHA512CryptoServiceProvider>();

            Provider = Services.BuildServiceProvider();
        }
        public static T GetService<T>()
        {
            return Provider.GetService<T>();
        }
    }
}
﻿using Pizzeria.ASP.DI;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class ProductModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [MinLength(2)]
        public string Name { get; set; }

        [MaxLength(1000)]
        [Required]
        public string Description { get; set; }

        public string Image { get; set; }

        [Required]
        [Range(0,10000)]
        public decimal Price { get; set; }
        public int? CategoryId { get; set; }

        private IEnumerable<CategoryModel> _allCategories;
        
        // Lazy Loading
        public IEnumerable<CategoryModel> AllCategories 
        {   get 
            {
                if (_allCategories != null) 
                    return _allCategories;
                CategoryRepository repo
                    = ServicesLocator.GetService<CategoryRepository>();
                IEnumerable<Category> r = repo.Get();
                return _allCategories = r.Select(c => new CategoryModel
                {
                    Name = c.Name,
                    Id = c.Id
                });
            } 
        }

        public HttpPostedFileBase File { get; set; }
    }
}
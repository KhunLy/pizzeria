﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string PlainPassword { get; set; }
    }
}